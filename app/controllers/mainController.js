
	app.controller('mainController',[
		'$scope','companiesService','clientsService','productsService','$q'
		,function($scope,companiesService,clientsService,productsService,$q) {
        	$scope.companies = [];
        	companiesService.getAll().then(function(response)
    		{
    			response.data.result.companies.forEach((e)=>{
    				$scope.companies.push(new company(e));
    			})
    		});

        	$scope.getCompanyDetails = function(company)
        	{
        		if(!company.knowDetails)
        		{
        			$q.all([clientsService.getById(company.id),
        				productsService.getById(company.id)])
	        		.then(function(response)
	        		{
	        			company.knowDetails = true;
	        			company.clients = response[0];
	        			company.products = response[1];
	        			$scope.selectedCompany = company.id;
	        		})	
        		}
        		else
        		{
        			$scope.selectedCompany = company.id;
        		}
        	}
        	
    }]);