
    class company{
    	constructor(data)
    	{
    		this.name = data.name;
    		this.id = data.id;
    		this.created = new Date(data.created);
    		this.offices = data.offices;
    		this._clients = [];
    		this.products = [];
    	}
    	set products(data)
    	{
    		this._products = data;
    	}
    	set knowDetails(e)
    	{
    		this._knowDetails = true;
    	}
    	get knowDetails()
    	{
    		return this._knowDetails == true;
    	}
    	get products()
    	{
    		return this._products;
    	}
    	set clients(data)
    	{
    		data.forEach((e)=>
    		{
    			this._clients.push(new client(e));
    		});
    	}
    	get clients()
    	{
    		return this._clients;
    	}

    	get defaultOffice()
    	{
    		return this.offices[0];
    	}
    	get phone()
    	{
    		return this.defaultOffice.phone;
    	}
    	get ubication()
    	{
    		var office = this.defaultOffice;
    		return {
    			country: office.country,
    			address:office.address.split(",").shift().trim(),
    			province: office.address.split(",").pop().trim()
    		}
    	}
    }