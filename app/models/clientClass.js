  class client{
    	constructor(data)
    	{
    		this.name = data.name;
    		this.id = data.id;
    		this.created = new Date(data.created);
    		this.offices = data.offices;
    	}

    	get defaultOffice()
    	{
    		return this.offices[0];
    	}
    	get phone()
    	{
    		return this.defaultOffice.phone;
    	}
    	get ubication()
    	{
    		var office = this.defaultOffice;
    		return {
    			country: office.country,
    			address:office.address.split(",").shift().trim(),
    			province: office.address.split(",").pop().trim()
    		}
    	}
    }