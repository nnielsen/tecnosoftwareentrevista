    var app = angular.module('app', [
    	'ngRoute'
    	]);

    let config = {
    	viewsDir:'app/views'
    }
    function viewResolver(view)
    {
    	return config.viewsDir + '/' + view + '.html';
    }

    app.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl : viewResolver('home'),
                controller  : 'mainController'
            })
    });