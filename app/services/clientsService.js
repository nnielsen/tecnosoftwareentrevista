 app.factory('clientsService',['$http',function($http)
    {
    	this.getById = function(id)
    	{
    		return $http.get('dataJson/clients'+id+'.json').
    		then(function(response)
    			{
    				return response.data.result.clients;
    			});
    	}
    	return this;
    }]);