  app.factory('productsService',['$http',function($http)
    {
    	this.getById = function(id)
    	{
    		return $http.get('dataJson/products'+id+'.json').
    		then(function(response)
    			{
    				return response.data.result.productos;
    			});;
    	}
    	return this;
    }]);